package me.ford.biomechanger.handlers;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Biome;
import org.bukkit.entity.Player;

import me.ford.biomechanger.BiomeChanger;
import me.ford.biomechanger.handlers.MessageHandler.Message;

public class ActiveBiomeChangeHandler {
	private final BiomeChanger BC;
	private Map<UUID, Biome> activeChangers = new HashMap<>();
	private Map<UUID, Set<Chunk>> activeChunks = new HashMap<>();
	private Set<Chunk> chunksToBeChanged = new HashSet<>();
	private final ChunkUpdater chunkUpdater;
	
	public ActiveBiomeChangeHandler(BiomeChanger plugin) {
		BC = plugin;
		chunkUpdater = new ChunkUpdater_1_13_2(); // TODO -> implementations for other versions
		int updateTicks = BC.getSettings().getUpdateTicks();
		BC.getServer().getScheduler().runTaskTimer(BC, new ChunkUpdateMonitor(this), updateTicks, updateTicks);
	}
	
	public void startChangingBiome(final Player player, final Biome biome, final long time, final double radius) {
		final UUID id = player.getUniqueId();
		activeChangers.put(id, biome);
		BC.getServer().getScheduler().runTaskLater(BC, () -> {
			stopChangingBiome(id);
		}, time * 20L);
		activeChunks.put(id, new HashSet<>());
		changeBiome(player, player.getLocation(), biome, radius);
	}
	
	public Biome stopChangingBiome(final Player player) {
		return stopChangingBiome(player.getUniqueId());
	}
	
	public Biome stopChangingBiome(final UUID id) {
		Biome biome = activeChangers.remove(id);
		Player player = BC.getServer().getPlayer(id);
		if (biome != null && player != null && player.isOnline()) {
			player.sendMessage(BC.getMessageHandler().getMessage(Message.STOP_CHANGE, "{biome}", biome.name()));
			Set<Chunk> chunks = activeChunks.get(id);
			chunksToBeChanged.addAll(chunks);
		}
		return biome;
	}
	
	public Biome isChangingTo(Player player) {
		return activeChangers.get(player.getUniqueId());
	}
	
	public boolean isChanging(Player player) {
		return isChangingTo(player) != null;
	}
	
	public void changeBiome(Player player, Location center, Biome changeTo) {
		changeBiome(player, center, changeTo, BC.getSettings().getBiomeChangeRadius());
	}
	
	public void changeBiome(Player player, Location center, Biome changeTo, double radius) {
		UUID id = player.getUniqueId();
		int centerX = center.getBlockX();
		int centerZ = center.getBlockZ();
		World world = center.getWorld();
		int r = (int) Math.ceil(radius);
		int r2 = r * r; // don't need to recalculate
		Set<Chunk> myChunks = activeChunks.get(id);
		if (myChunks == null) myChunks = new HashSet<>();
		for (int x = -r; x <= r; x++) {
			int x2 = x * x; // don't need to recalculate for each z
			int curX = centerX + x;
			for (int z = -r; z <= r; z++) {
				int curZ = centerZ + z;
				if (x2 + z*z <= r2) { // within circle
					if (world.getBiome(curX, curZ) != changeTo) {
						world.setBiome(curX, curZ, changeTo);
						myChunks.add(world.getChunkAt(curX >> 4, curZ >> 4));
					}
				}
			}
		}
		for (Chunk chunk : myChunks) {
			if (chunk != center.getChunk()) {
				Set<Chunk> myChunk = new HashSet<>();
				myChunk.add(chunk);
				
			}
		}
		activeChunks.put(id,  myChunks);
	}
	
	public void playerMovedFromChunk(Player player, Chunk chunk) {
		BC.getServer().getScheduler().runTask(BC, () -> { // run on NEXT tick
			UUID id = player.getUniqueId();
			if (!activeChunks.containsKey(id)) {
				return;
			}
			if (!activeChunks.get(id).contains(chunk)) {
				return;
			}
			if (!chunkUpdater.updateChunk(chunk, player)) { // if success
				activeChunks.get(id).remove(chunk);
				if (activeChunks.get(id).isEmpty()) { // if none left
					activeChunks.remove(id);
				}
			}
		});
	}
	
	public boolean hasChunksToChange(Player player) {
		return activeChunks.containsKey(player.getUniqueId());
	}
	
	protected Set<Chunk> getChunksToBeChanged() {
		return chunksToBeChanged;
	}
	
	protected void setChunksToBeChanged(Set<Chunk> chunks) {
		chunksToBeChanged = chunks;
	}
	
	protected ChunkUpdater getChunkUpdater() {
		return chunkUpdater;
	}
	
	protected BiomeChanger getPlugin() {
		return BC;
	}
	
	
	// this will update the chunks for players other than the one chanign the biomes
	// but only after the changes are done (i.e time has run out)
	public static class ChunkUpdateMonitor implements Runnable {
		private final ActiveBiomeChangeHandler handler;
		
		public ChunkUpdateMonitor(ActiveBiomeChangeHandler handler) {
			this.handler = handler;
		}

		@Override
		public void run() {
			Set<Chunk> chunks = handler.getChunksToBeChanged();
			chunks = handler.getChunkUpdater().updateChunks(chunks, handler.getPlugin().getServer().getOnlinePlayers());
			handler.setChunksToBeChanged(chunks);
		}
		
	}
	

}
