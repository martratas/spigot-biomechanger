package me.ford.biomechanger.commands;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.block.Biome;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import me.ford.biomechanger.BiomeChanger;
import me.ford.biomechanger.handlers.MessageHandler.Message;

public class AbortBiomeChangeCommand implements TabExecutor {
	private final BiomeChanger BC;
	
	public AbortBiomeChangeCommand(BiomeChanger plugin) {
		BC = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length < 1) {
			return false;
		}
		Player target = BC.getOnlinePlayer(args[0]);
		if (target == null) {
			sender.sendMessage(BC.getMessageHandler().getMessage(Message.PLAYER_NOT_FOUND, "{player}", args[0]));
			return true;
		}
		if (!BC.getBiomeChangeHandler().isChanging(target)) {
			sender.sendMessage(BC.getMessageHandler().getMessage(Message.PLAYER_NOT_CHANGING, "{player}", target.getName()));
			return true;
		}
		Biome biome = BC.getBiomeChangeHandler().stopChangingBiome(target);
		if (biome == null) {
			BC.getLogger().warning("Player is changing biomes, but biome is null...");
			return true;
		}
		sender.sendMessage(BC.getMessageHandler().getMessage(Message.STOPPED_PLAYER_CHANGING, 
											"{player}", target.getName(),
											"{biome}", biome.name()));
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		if (args.length == 1) return null; // default to playerlist
		return new ArrayList<>();
	}

}
