package me.ford.biomechanger.handlers;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_14_R1.CraftChunk;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_14_R1.PacketPlayOutMapChunk;
import net.minecraft.server.v1_14_R1.PacketPlayOutUnloadChunk;
import net.minecraft.server.v1_14_R1.PlayerConnection;

public class ChunkUpdater_1_14_4 implements ChunkUpdater {
	
	@Override
	public Set<Chunk> updateChunksForCloseBy(Set<Chunk> chunks, Location center, double radius) {
		return updateChunksForCloseBy(chunks, center, radius, false);
	}

	@Override
	public Set<Chunk> updateChunksForCloseBy(Set<Chunk> chunks, Location center, double radius, boolean force) {
		Set<Player> players = new HashSet<>();
		for (Entity e : center.getWorld().getNearbyEntities(center, radius, radius, radius)) {
			if (e instanceof Player) {
				players.add((Player) e);
			}
		}
		return updateChunks(chunks, players, force);
	}

	@Override
	public Set<Chunk> updateChunks(Set<Chunk> chunks, Collection<? extends Player> players) {
		return updateChunks(chunks, players, false);
	}
	
	@Override
	public Set<Chunk> updateChunks(Set<Chunk> chunks, Collection<? extends Player> players, boolean force) {
		Set<Chunk> notUpdated = new HashSet<>();
		chunkLoop:
		for (Chunk chunk : chunks) {
			if (!force) { // if not forcing, then skipping player occupied chunks
				for (Entity e : chunk.getEntities()) {
					if (e instanceof Player) { // if occupied by player, not updating
						notUpdated.add(chunk); 
						continue chunkLoop; // next chunk
					}
				}
			}
			// sending update to players
			for (Player player : players) {
				if (player.getWorld() == chunk.getWorld()) {
					updateChunkFor(player, chunk);
				} else {
				}
			}
		}
		return notUpdated;
	}
	
	
	private void updateChunkFor(Player player, Chunk chunk) {
		net.minecraft.server.v1_14_R1.Chunk nmsChunk = ((CraftChunk) chunk).getHandle();
		PacketPlayOutUnloadChunk unload = new PacketPlayOutUnloadChunk(chunk.getX(), chunk.getZ());
		PacketPlayOutMapChunk load = new PacketPlayOutMapChunk(nmsChunk, 65535);
		PlayerConnection conn = ((CraftPlayer) player).getHandle().playerConnection;
        conn.sendPacket(unload);
        conn.sendPacket(load);
	}

	@Override
	public boolean updateChunk(Chunk chunk, Player player) {
		return updateChunk(chunk, player, false);
	}

	@Override
	public boolean updateChunk(Chunk chunk, Player player, boolean force) {
		Set<Chunk> curChunk = new HashSet<>();
		curChunk.add(chunk);
		return updateChunks(curChunk, Arrays.asList(player), force).isEmpty();
	}

}
