package me.ford.biomechanger;

import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import me.ford.biomechanger.commands.AbortBiomeChangeCommand;
import me.ford.biomechanger.commands.ChangeBiomeCommand;
import me.ford.biomechanger.handlers.ActiveBiomeChangeHandler;
import me.ford.biomechanger.handlers.MessageHandler;
import me.ford.biomechanger.listeners.MoveListener;

public class BiomeChanger extends JavaPlugin {
	private Settings settings;
	private ActiveBiomeChangeHandler changeHandler;
	private MessageHandler messageHandler;
	
	public void onEnable() {
		loadConfiguration();
		
		// handlers
		changeHandler = new ActiveBiomeChangeHandler(this);
		messageHandler = new MessageHandler(getConfig().getConfigurationSection("messages"));
		
		// commands
		getCommand("changebiome").setExecutor(new ChangeBiomeCommand(this));
		getCommand("abortbiomechange").setExecutor(new AbortBiomeChangeCommand(this));
		
		// listeners
		getServer().getPluginManager().registerEvents(new MoveListener(this), this);
		
	}
	
	private void loadConfiguration() {
		getConfig().options().copyDefaults(true);
		saveConfig();
		settings = new Settings(getConfig());
	}
	
	public void reload() {
		reloadConfig();
		settings.loadSettings(getConfig());
		messageHandler.loadMessages(getConfig().getConfigurationSection("messages"));
	}
	
	public Settings getSettings() {
		return settings;
	}
	
	public ActiveBiomeChangeHandler getBiomeChangeHandler() {
		return changeHandler;
	}
	
	public MessageHandler getMessageHandler() {
		return messageHandler;
	}
	
	public Player getOnlinePlayer(String name) {
		for (Player player : getServer().getOnlinePlayers()) {
			if (player.getName().equalsIgnoreCase(name)) {
				// TODO - vanish checks
				return player;
			}
		}
		return null;
	}

}
