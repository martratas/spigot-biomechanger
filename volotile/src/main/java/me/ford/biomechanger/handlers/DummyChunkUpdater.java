
package me.ford.biomechanger.handlers;

import java.util.Collection;
import java.util.Set;
import java.util.HashSet;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.entity.Player;


public class DummyChunkUpdater implements ChunkUpdater {
	
	@Override
	public Set<Chunk> updateChunksForCloseBy(Set<Chunk> chunks, Location center, double radius) {
		return updateChunksForCloseBy(chunks, center, radius, false);
	}

	@Override
	public Set<Chunk> updateChunksForCloseBy(Set<Chunk> chunks, Location center, double radius, boolean force) {
		return new HashSet<>();
	}

	@Override
	public Set<Chunk> updateChunks(Set<Chunk> chunks, Collection<? extends Player> players) {
		return updateChunks(chunks, players, false);
	}
	
	@Override
	public Set<Chunk> updateChunks(Set<Chunk> chunks, Collection<? extends Player> players, boolean force) {
		return new HashSet<>();
	}
	
	
	private void updateChunkFor(Player player, Chunk chunk) {
	}

	@Override
	public boolean updateChunk(Chunk chunk, Player player) {
		return updateChunk(chunk, player, false);
	}

	@Override
	public boolean updateChunk(Chunk chunk, Player player, boolean force) {
		return true;
    }
}