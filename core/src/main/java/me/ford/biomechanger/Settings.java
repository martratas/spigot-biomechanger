package me.ford.biomechanger;

import org.bukkit.configuration.ConfigurationSection;

public class Settings {
	private ConfigurationSection conf;
	
	public Settings(ConfigurationSection conf) {
		loadSettings(conf);
	}
	
	public void loadSettings(ConfigurationSection conf) {
		this.conf = conf;
	}
	
	public double getBiomeChangeRadius() {
		return conf.getDouble("biome-change.radius", 5.0D);
	}
	
	public int getUpdateTicks() {
		return conf.getInt("biome-change.update-chunks-ticks", 100);
	}

}
