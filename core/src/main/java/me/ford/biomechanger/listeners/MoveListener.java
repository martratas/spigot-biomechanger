package me.ford.biomechanger.listeners;

import org.bukkit.Chunk;
import org.bukkit.block.Biome;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import me.ford.biomechanger.BiomeChanger;

public class MoveListener implements Listener {
	private final BiomeChanger BC;
	
	public MoveListener(BiomeChanger plugin) {
		BC = plugin;
	}
	
	@EventHandler
	public void onMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		if (event.getFrom().distanceSquared(event.getTo()) == 0) {
			return; // didn't move
		}
		if (BC.getBiomeChangeHandler().hasChunksToChange(player)) { // if they need chunks changed
			Chunk from = event.getFrom().getChunk();
			Chunk to = event.getTo().getChunk();
			if (from != to) {
				BC.getBiomeChangeHandler().playerMovedFromChunk(event.getPlayer(), from);
			}
		}
		Biome changeTo = BC.getBiomeChangeHandler().isChangingTo(player);
		if (changeTo == null) {
			return; // not changing biomes
		}
//		Player player = event.getPlayer();
		BC.getBiomeChangeHandler().changeBiome(event.getPlayer(), event.getTo(), changeTo);
	}

}
