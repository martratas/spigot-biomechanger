package me.ford.biomechanger.handlers;

import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;

public class MessageHandler {
	private ConfigurationSection conf;
	
	public MessageHandler(ConfigurationSection conf) {
		loadMessages(conf);
	}
	
	public void loadMessages(ConfigurationSection conf) {
		this.conf = conf;
	}
	
	public String getMessage(Message message, String...replacements) {
		String msg = conf.getString(message.getPath(), message.getDefault());
		for (int i = 0; i + 1 < replacements.length; i+=2) {
			msg = msg.replace(replacements[i], replacements[i+1]);
		}
		return ChatColor.translateAlternateColorCodes('&', msg);
	}
	
	public static enum Message {
		NO_SUCH_BIOME("no-such-biome", "&cNo biome named &7{biome}&c!"),
		TIME_MUST_BE_NUMBER("time-must-be-number", "&cTime inserted must ba a number in seconds"),
		PLAYER_NOT_FOUND("player-not-found", "&cThe player &7{player}&c was not found!"),
		CONSOLE_NEEDS_TARGET("console-needs-target", "&c console needs to specify a target player!"),
		STARTING_TO_CHANGE("starting-to-change", "&6Now changing the biome around you to &8{biome}&6 for &7{time}&6 seconds"),
		STARTING_TO_CHANGE_OTHER("starting-to-change-other", "&6Now changing the biome around &7{player}&6 to &8{biome}&6 for &7{time}&6 seconds"),
		STOP_CHANGE("stop-changing-biome", "&6Now stopping the changing of the biome to &7{biome}&6.\nYou may need to move around to see the changes"),
		ALREADY_CHANGING("already-changing", "&cAlready changing a biome!"),
		ALREADY_CHANGING_OTHER("already-changing-other", "&7{player}&c is already changing a biome!"),
		PLAYER_NOT_CHANGING("player-not-changing", "&7{player}&c is not changing a biome right now!"),
		STOPPED_PLAYER_CHANGING("stopped-player-changing", "&6Stopped &7{player}&6 changing their biome to &8{biome}"),
		NO_PERMS_FOR_BIOME("no-perms-for-biome", "&cYou do not have permissions for this biome: &8{biome}")
		;
		
		private final String path;
		private final String def;
		
		Message(String path, String def) {
			this.path = path;
			this.def = def;
		}
		
		public String getPath() {
			return path;
		}
		
		public String getDefault() {
			return def;
		}
	}

}
