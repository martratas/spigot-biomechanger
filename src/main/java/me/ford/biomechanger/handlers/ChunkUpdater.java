package me.ford.biomechanger.handlers;

import java.util.Collection;
import java.util.Set;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public interface ChunkUpdater {

	public Set<Chunk> updateChunks(Set<Chunk> chunks, Collection<? extends Player> players, boolean force);
	
	public Set<Chunk> updateChunks(Set<Chunk> chunks, Collection<? extends Player> collection);

	public boolean updateChunk(Chunk chunk, Player player);
	
	public boolean updateChunk(Chunk chunk, Player player, boolean force);

	public Set<Chunk> updateChunksForCloseBy(Set<Chunk> chunks, Location center, double radius);
	
	public Set<Chunk> updateChunksForCloseBy(Set<Chunk> chunks, Location center, double radius, boolean force);

}
