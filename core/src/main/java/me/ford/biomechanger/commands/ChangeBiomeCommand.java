package me.ford.biomechanger.commands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.block.Biome;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;
import org.bukkit.util.StringUtil;

import me.ford.biomechanger.BiomeChanger;
import me.ford.biomechanger.handlers.MessageHandler.Message;

public class ChangeBiomeCommand implements TabExecutor {
	// /<command> <biome> <time> [player] [radius]
	private final BiomeChanger BC;
	private final List<String> biomes;
	
	public ChangeBiomeCommand(BiomeChanger plugin) {
		BC = plugin;
		biomes = Arrays.asList(Biome.values()).stream().map(Biome::name).collect(Collectors.toList());
		// TODO - remove unwanted biomes
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (args.length < 2) {
			return false;
		}
		
		// get biome
		Biome biome;
		try {
			biome = Biome.valueOf(args[0].toUpperCase());
		} catch (IllegalArgumentException e) {
			sender.sendMessage(BC.getMessageHandler().getMessage(Message.NO_SUCH_BIOME, "{biome}", args[0]));
			return true;
		}
		
		// check biome-specific permissions
		if (!sender.hasPermission("biomechanger.change.*") &&
				!sender.hasPermission("biomechanger.change." + biome.name())) {
			sender.sendMessage(BC.getMessageHandler().getMessage(Message.NO_PERMS_FOR_BIOME, "{biome}", biome.name()));
			return true;
		}
		
		// get time
		int time;
		try {
			time = Integer.parseInt(args[1]);
		} catch (NumberFormatException e) {
			sender.sendMessage(BC.getMessageHandler().getMessage(Message.TIME_MUST_BE_NUMBER));
			return true;
		}
		
		// get target
		String targetName = "";
		Player target = null;
		if (args.length > 2 &&
				(sender.hasPermission("biomechanger.change.others.*") ||
				sender.hasPermission("biomechanger.change.others." + biome.name()))) {
			targetName = args[2];
			target = BC.getOnlinePlayer(targetName);
		}
		// if no target specified (or allowed), revert to sender if possible
		if (target == null && sender instanceof Player) {
			target = (Player) sender;
		}
		
		// handle no specified target 
		if (target == null && !targetName.isEmpty()) {
			sender.sendMessage(BC.getMessageHandler().getMessage(Message.PLAYER_NOT_FOUND, "{player}", targetName));
			return true;
		} else if (target == null) {
			sender.sendMessage(BC.getMessageHandler().getMessage(Message.CONSOLE_NEEDS_TARGET));
			return false;
		}
		
		// get radius if needed
		double radius = BC.getSettings().getBiomeChangeRadius();
		if (sender.hasPermission("biomechanger.change.radius") && args.length > 3) {
			try {
				radius = Double.parseDouble(args[3]);
			} catch (NumberFormatException e) { } // remains at default
		}
		
		if (BC.getBiomeChangeHandler().isChanging(target)) {
			if (target == sender) {
				sender.sendMessage(BC.getMessageHandler().getMessage(Message.ALREADY_CHANGING));
			} else {
				sender.sendMessage(BC.getMessageHandler().getMessage(Message.ALREADY_CHANGING_OTHER, 
													"{player}", target.getName()));
			}
			return true;
		}
		
		// start changing the biome
		BC.getBiomeChangeHandler().startChangingBiome(target, biome, time, radius);
		
		// handle success message depending on whether the target is different from the sender
		target.sendMessage(BC.getMessageHandler().getMessage(Message.STARTING_TO_CHANGE,
				"{biome}", biome.name(),
				"{time}", "" + time));
		if (sender != target) {
			sender.sendMessage(BC.getMessageHandler().getMessage(Message.STARTING_TO_CHANGE_OTHER, 
					"{player}", target.getName(),
					"{biome}", biome.name(),
					"{time}", "" + time));
		}
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		List<String> list = new ArrayList<>();
		if (args.length == 1) {
			return StringUtil.copyPartialMatches(args[0], biomes, list);
		} else if (args.length == 2) {
			return list;
		} else if (args.length == 3) {
			return null;
		}
		return list;
	}

}
